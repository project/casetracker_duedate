
Introduction
------------
Case Tracker Due Date adds a due date field to the `casetracker_basic_case` content type and modifies existing views to include due dates for tasks/cases. If used with Open Atrium, extra integration is available.


Features
--------
Any system:

 * Adds a CCK "Due Date" field to the `casetracker_basic_case` type.
 * Adds output filters to display due dates as "today", "tomorrow", "x days", or the actual date depending on how far off it is.
 * Modify primary case listing table to include a "Due Date" column.
 * Adds ability to filter cases by due date.
 * Provides "Upcoming cases" page and block.

Open Atrium systems only:

 * Modifies calendar view to show cases with due dates.
 * Adds "Upcoming cases" blocks to casetracker and calendar contexts.


Similar projects
----------------
This module works well in conjunction with [Case Tracker Comment Driven](http://drupal.org/project/casetracker_comment_driven) to allow due dates to be modified like other Case Tracker fields.


Limitations
------------
This module requires you to be using the content types provided by the `casetracker_basic` module (or the `atrium_casetracker` if you are on Open Atrium). If you have assigned other types as cases, they are not eligible for the functionality provided by this module.

**NOTE:** If you are using this module with Open Atrium, DO NOT enable `casetracker_basic` and `atrium_casetracker` at the same time.

Installation
------------
Dependencies:

 * Casetracker
 * Date
 * Features

Requirements:

 * features-6.x-beta11 or higher.
 * (If using with Open Atrium) Open Atrium Beta 8 or higher

Installation

 * Enable `casetracker_duedate`
 * Enable EITHER `casetracker_basic` (for non-Open Atrium sites) OR `atrium_casetracker` (for Open Atrium sites) but not both

TODO
----
Immediately:

 * When sorting table by due date, cases without a due date float to the top.
 * Possibly rename "upcoming cases" to something more intuitive

Soonish:

 * Atrium: Add "recent tasks" to sidebar of calendar view since they are showing up on the calendar

Later:

 * Allow user to specify a view that will be used other than atrium_calendar (they might have a cloned version or some other calendar). Possibly let them specify multiple calendars.
 * Atrium: Color calendar items based on their project color


Development
-----------
This module was initially developed for [Google Summer of Code 2010](http://drupal.org/node/782294) by [auzigog](http://drupal.org/user/542166) (Jeremy Blanchard). Further development is occurring as part of a set of grassroots project management tools being developed at [Activism Labs](http://activismlabs.org).

http://activismlabs.org